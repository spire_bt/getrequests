//
//  BaseRequest.h
//  TechnicalAssignment
//
//  Created by Spire Jankulovski on 8/22/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BaseRequestDelegate <NSObject>
- (void)getDataFromRequest:(NSString*) dataOne forRequest:(NSURLRequest *)request;
@end



@interface BaseRequest : NSObject{
       __weak id <BaseRequestDelegate> delegate;
}
@property (nonatomic, weak)  id delegate;

- (void)initRequestsFromArray:(NSMutableArray*)requestsArray;

@end
