//
//  BaseRequest.m
//  TechnicalAssignment
//
//  Created by Spire Jankulovski on 8/22/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "BaseRequest.h"

@implementation BaseRequest
@synthesize delegate;

- (id)init
{
    if (self = [super init]) {
        
    }
    return self;
}
- (void)initRequestsFromArray:(NSMutableArray*)requestsArray
{
    //Creating counter to indicate when requests are processed (if needed)
    __block NSInteger outstandingRequests = [requestsArray count];
    
    
    //This is code that sends synchronous requests using asynchronous dispatch queue
    //Getting the global concurrent queue
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //For each request in array
    for (NSURLRequest *request in requestsArray) {
        //dispatch asynchronous queue with
        dispatch_async(queue, ^{
            NSURLResponse *response;
            NSError *error;
            //a synchronus requests
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if ([data length] > 0 && error == nil)
                [self receivedData:data andResponse:response andRequest:request];
            else if ([data length] == 0 && error == nil)
                [self emptyReply];
            else if (error != nil && error.code == NSURLErrorTimedOut)
                [self timedOut];
            else if (error != nil)
                [self downloadError:error];
            
            outstandingRequests--;
            if (outstandingRequests == 0) {
                [self lastResponseReceived];
            }
            
        });
    }

}
-(void)receivedData:(NSData *)data andResponse:(NSURLResponse*)response andRequest:(NSURLRequest *)request{
    //Converting the received data in string
    NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
    //returning the data string and the request to the main view
    [[self delegate] getDataFromRequest:newStr forRequest:request];
}
-(void)lastResponseReceived{
    //Processed all responses
    NSLog(@"Responses received");

}
-(void)emptyReply{
    NSLog(@"Empty Reply");
}

-(void)timedOut{
    
    NSLog(@"Timed Out");
}

-(void)downloadError:(NSError *)error{
    
    NSLog(@"downloadError %@",error);
}

@end
