//
//  OperationsController.h
//  TechnicalAssignment
//
//  Created by Spire Jankulovski on 8/22/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OperationsControllerDelegate <NSObject>
-(void)find10thCharacter:(NSString *)character atIndex:(int)index;
-(void)findEvery10thCharacter:(NSMutableArray *)characters;
-(void)wordCounter:(NSArray *)allWords andCount:(long long)count forWord:(NSString *)countString;
@end

@interface OperationsController : NSObject{
    __weak id <OperationsControllerDelegate> delegate;
}
-(void)operationsFor10thCharacter:(NSString *)webString;
-(void)operationsForEvery10thCharacter:(NSString *)webString;
-(void)operationsForWordCounter:(NSString *)webString calculateForWord:(NSString *)countString;
@property (nonatomic, weak)  id delegate;

@end
