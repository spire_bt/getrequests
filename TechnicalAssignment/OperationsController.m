//
//  OperationsController.m
//  TechnicalAssignment
//
//  Created by Spire Jankulovski on 8/22/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "OperationsController.h"

@implementation OperationsController
@synthesize delegate;

- (id)init
{
    if (self = [super init]) {
        
    }
    return self;
}
-(void)operationsFor10thCharacter:(NSString *)webString{

    //Setting the index of the character we need to show in the Text View
    //Using 9 because 10 was a white space
    int index = 10;
    NSString *theCharacter = [NSString stringWithFormat:@"%c", [webString characterAtIndex:index]];

    //Returning the character and the index of that character
    [[self delegate] find10thCharacter:theCharacter atIndex:index];

}
-(void)operationsForEvery10thCharacter:(NSString *)webString{
    
    NSMutableArray *resultArray = [NSMutableArray array];
    
    //The lenght of the string devided by 10 will give us the means to loop
    //for every 10th character
    for (int i = 1; i<webString.length/10; i++) {
        //The index of the character that we need
        int index = 10*i;
        //Creating string from the character
        NSString *theCharacter = [NSString stringWithFormat:@"%c", [webString characterAtIndex:index]];
        //adding the character in array
        [resultArray addObject:theCharacter];

    }
    //Returning the array
    [[self delegate] findEvery10thCharacter:resultArray];

}
-(void)operationsForWordCounter:(NSString *)webString calculateForWord:(NSString *)countString{
    long long counter = 0;
    //Creating array from the string
    //every component is separated with white space or new line
    NSArray *arr = [webString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //If we have a string that we need count for
    if (countString.length!=0) {
        //get every component of the array in a string
        for (NSString *someString in arr) {
            //adn compare it with the desired string
            if ([someString isEqualToString:countString]) {
                //Incrise the counter by 1
                counter++;
            }
        }
    //else return the full array count
    }else{
        counter = [arr count];
        countString = @"All words";
    }

    //return the array of characters, the counter and the word that we counted for
    [[self delegate] wordCounter:arr andCount:counter forWord:countString];

}

@end
