//
//  ViewController.h
//  TechnicalAssignment
//
//  Created by Spire Jankulovski on 8/22/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseRequest.h"
#import "OperationsController.h"

@interface ViewController : UIViewController{
    NSMutableArray *requestArray;
}
//Method that starts the requests
-(IBAction)startRequests:(id)sender;
//Button that calls the "startRequest" method
@property (weak, nonatomic) IBOutlet UIButton *startButton;

//Activity indicators for the requests
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorForCharacterRequest;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorForEvery10thCharacterRequest;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorForWordCount;

//Text Views for request results
@property (weak, nonatomic) IBOutlet UITextView *characterRequest;
@property (weak, nonatomic) IBOutlet UITextView *everyCharacterRequest;
@property (weak, nonatomic) IBOutlet UITextView *wordCounter;

//Array that holds teh requests
@property (strong, nonatomic)  NSMutableArray *requestArray;
@end

