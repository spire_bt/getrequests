//
//  ViewController.m
//  TechnicalAssignment
//
//  Created by Spire Jankulovski on 8/22/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "ViewController.h"
@interface ViewController ()

@end

@implementation ViewController

@synthesize startButton;
@synthesize requestArray;
@synthesize activityIndicatorForWordCount,activityIndicatorForEvery10thCharacterRequest,activityIndicatorForCharacterRequest;

- (void)viewDidLoad {
    [super viewDidLoad];
    //Hiding the Activity indicators on application start
    [self.activityIndicatorForCharacterRequest setHidden:YES];
    [self.activityIndicatorForEvery10thCharacterRequest setHidden:YES];
    [self.activityIndicatorForWordCount setHidden:YES];
}
-(IBAction)startRequests:(id)sender{
    /*First Activity indicator*/
    //Check if activity indicator is hidden
    if (self.activityIndicatorForCharacterRequest.hidden) {
        //show activity indicator
        [self.activityIndicatorForCharacterRequest setHidden:NO];
    }
    //Hide activity indicator when stoped, and start animating
    self.activityIndicatorForCharacterRequest.hidesWhenStopped = YES;
    [self.activityIndicatorForCharacterRequest startAnimating];

    /*Second Activity indicator*/
    //Check if activity indicator is hidden
    if (self.activityIndicatorForEvery10thCharacterRequest.hidden) {
        //show activity indicator
        [self.activityIndicatorForEvery10thCharacterRequest setHidden:NO];
    }
    //Hide activity indicator when stoped, and start animating
    self.activityIndicatorForEvery10thCharacterRequest.hidesWhenStopped = YES;
    [self.activityIndicatorForEvery10thCharacterRequest startAnimating];

    /*Third Activity indicator*/
    //Check if activity indicator is hidden
    if (self.activityIndicatorForWordCount.hidden) {
        //show activity indicator
        [self.activityIndicatorForWordCount setHidden:NO];
    }
    //Hide activity indicator when stoped, and start animating
   self.activityIndicatorForWordCount.hidesWhenStopped = YES;
    [self.activityIndicatorForWordCount startAnimating];

    
    //Setting the url for the requests
    NSURL *url = [NSURL URLWithString:@"http://www.website.com/"];
    
    NSMutableURLRequest *truecaller10thCharacterRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [truecaller10thCharacterRequest setHTTPMethod:@"GET"];

    NSMutableURLRequest *truecallerEvery10thCharacterRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [truecallerEvery10thCharacterRequest setHTTPMethod:@"GET"];

    
    NSMutableURLRequest *truecallerWordCounterRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [truecallerWordCounterRequest setHTTPMethod:@"GET"];
    
    
    //Add created requests in array
    requestArray = [NSMutableArray arrayWithObjects:truecaller10thCharacterRequest, truecallerEvery10thCharacterRequest, truecallerWordCounterRequest, nil];

    //Initialization of the request class
    BaseRequest *baseRequest = [[BaseRequest alloc] init];
    //Setting delegate
    [baseRequest setDelegate:self];
    //Invoking request method with the request array
    [baseRequest initRequestsFromArray:requestArray];
    
    
}
- (void)getDataFromRequest:(NSString*)webString forRequest:(NSURLRequest *)request{
    
    //Initialization of the operations class
    OperationsController *operationsController = [[OperationsController alloc]init];
    //Setting delegate
    [operationsController setDelegate:self];

    //Setting the searched word for the 3rd request
    NSString *countWord = [NSString stringWithFormat:@"Software"];
    
    //Checking if the first object of the request array is the returned response
    if ([requestArray objectAtIndex:0]==request) {
        //If it is than this webString should be processed in operationsFor10thCharacter
        [operationsController operationsFor10thCharacter:webString];
    }else if ([requestArray objectAtIndex:1]==request) {
        //Checking if the second object of the request array is the returned response
        //If it is than this webString should be processed in operationsForEvery10thCharacter
        [operationsController operationsForEvery10thCharacter:webString];
    }else if ([requestArray objectAtIndex:2]==request) {
        //Checking if the third object of the request array is the returned response
        //If it is than this webString should be processed in operationsForWordCounter
        [operationsController operationsForWordCounter:webString calculateForWord:countWord];
    }
}
-(void)find10thCharacter:(NSString *)character atIndex:(int)index{
    
    NSString *endString = [NSString stringWithFormat:@"The %dth character is %@", index,character];

    //Dispathc asynchronous on main thread
    //Set text to Text View
    //Activity Indicator stop animating
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.characterRequest setText:endString];
        [self.activityIndicatorForCharacterRequest stopAnimating];
    });

}
-(void)findEvery10thCharacter:(NSMutableArray *)characters{

    NSString * result =[NSString stringWithFormat:@"- %@",[[characters valueForKey:@"description"] componentsJoinedByString:@"\n- "]];

    //Dispathc asynchronous on main thread
    //Set text to Text View
    //Activity Indicator stop animating
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.everyCharacterRequest setText:result];
        [self.activityIndicatorForEvery10thCharacterRequest stopAnimating];
    });


}

-(void)wordCounter:(NSArray *)allWords andCount:(long long)count forWord:(NSString *)countString{

    NSString *endString = [NSString stringWithFormat:@"\"%@\" on the website : %lld",countString, count];

    //Dispathc asynchronous on main thread
    //Set text to Text View
    //Activity Indicator stop animating
    dispatch_async(dispatch_get_main_queue(), ^{
       [self.wordCounter setText:endString];
       [self.activityIndicatorForWordCount stopAnimating];
    });


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
